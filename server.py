import socket
import sys
import random
import time

# Function to simulate packet loss
def simulate_packet_loss(loss_rate):
    return random.uniform(0.1, 0.9) >= loss_rate

def main():
    port = 12346
    chunk_size = 100
    loss_rate = 0.5
    Rn = 0 # Receive Number

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind(('127.0.0.1', port))

    print(f"Server is listening on port {port} with chunk size {chunk_size} and loss rate {loss_rate}")

    received_data = b""  # Store received data

    while True:
        data, client_address = server_socket.recvfrom(chunk_size)
        seq_no = int.from_bytes(data[4:8], byteorder='big')
        if simulate_packet_loss(loss_rate) and Rn == seq_no:
            server_socket.sendto(data, client_address)  # Acknowledge the received packet
            print(f"Received {len(data)} bytes from {client_address}")
            received_data += data[12:]  # Add received data to the variable received_data
            print("Result: ", received_data)
            Rn += 1
        else:
            # Sleep
            print(f"Packet loss, sequence number {len(data)} bytes from {client_address}")

if True:
    main()
    