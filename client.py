import socket
import sys
import time

class GBNStruct:
    #เป็นคลาส nested ภายในโปรแกรม โดยคลาสนี้ represent 
    #แพ็กเก็ตข้อมูลที่ใช้กับโปรโตคอล Go-Back-N คลาสมี 5 attributes
    def __init__(self, seq_no, ack_no, data_size, data):
        self.seq_no = seq_no #seq_no: หมายเลขลำดับของแพ็กเก็ต
        self.ack_no = ack_no #ack_no: หมายเลข acknowledgment ของแพ็กเก็ต
        self.data_size = data_size #data_size: ขนาดของข้อมูล payload
        self.data = data #data: ข้อมูล payload ของแพ็กเก็ต
        self.is_acknowledged = False #is_acknowledged: สถานะ acknowledgment ของแพ็กเก็ต
        self.last_sent_time = None

    def serialize(self): #แปลงอินสแตนซ์ GBNStruct เป็น byte string สำหรับการส่ง โดยจะเพิ่มหมายเลขลำดับเป็น string 3 หลักต่อท้ายข้อมูล
        seq_no_bytes = self.seq_no.to_bytes(4, byteorder='big') #แปลงหมายเลขลำดับเป็น string 3 หลัก
        ack_no_bytes = self.ack_no.to_bytes(4, byteorder='big') #แปลงหมายเลข acknowledgment เป็น string 3 หลัก
        data_size_bytes = self.data_size.to_bytes(4, byteorder='big') #แปลงขนาดของข้อมูล payload เป็น string 3 หลัก
        serialized_data = seq_no_bytes + ack_no_bytes + data_size_bytes + self.data #concat ข้อมูลทั้งหมดเข้าด้วยกัน
        return serialized_data

    @staticmethod
    def deserialize(data): #ใช้เพื่อสร้างอินสแตนซ์ GBNStruct จาก byte string ที่รับมา โดยจะแยกหมายเลขลำดับจาก 3 ไบต์แรก และข้อมูลจากส่วนที่เหลือของสตริง
        seq_no = int.from_bytes(data[0:4], byteorder='big') #แยกหมายเลขลำดับออกจาก byte string
        ack_no = int.from_bytes(data[4:8], byteorder='big') #แยกหมายเลข acknowledgment ออกจาก byte string
        data_size = int.from_bytes(data[8:12], byteorder='big') #แยกขนาดของข้อมูล payload ออกจาก byte string
        packet_data = data[12:] #แยกข้อมูล payload ออกจาก byte string
        return GBNStruct(seq_no, ack_no, data_size, packet_data) #สร้างอินสแตนซ์ GBNStruct โดยใช้ข้อมูลที่ได้รับ

def main():
    server_ip = "127.0.0.1" # IP address ของเซิร์ฟเวอร์
    server_port = 12346 #Port number ของเซิร์ฟเวอร์
    chunk_size = 2 #ขนาดของ chunk ที่จะอ่านจากไฟล์
    window_size = 4 #จำนวนแพ็กเก็ตที่ยังไม่ได้รับการ acknowledgment สูงสุด

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # socket UDP สำหรับส่งและรับข้อมูล
    client_socket.settimeout(0.1) 

    packets = [] #List ของอินสแตนซ์ GBNStruct ที่ represent แพ็กเก็ตข้อมูล
    base = 0 #หมายเลขลำดับของแพ็กเก็ตที่ยังไม่ได้รับการ acknowledgment ที่เก่าแก่ที่สุด
    next_seq_num = 0 #หมายเลขลำดับของแพ็กเก็ตถัดไปที่จะส่ง
    timeout = 2 #ระยะเวลาที่รอ acknowledgment ก่อนส่งแพ็กเก็ตอีกครั้ง
    trys = 10 #จำนวนครั้งที่จะส่งแพ็กเก็ตอีกครั้งก่อนที่จะยอมแพ้

    with open('filetosend.txt', 'rb') as file: #อ่านไฟล์ filetosend.txt เป็น chunk และสร้างอินสแตนซ์ GBNStruct สำหรับแต่ละ chunk
        while True: # วนซ้ำจนกว่า EOF (End of File)
            data = file.read(chunk_size) #อ่านข้อมูลในไฟล์เป็น chunk ขนาด
            if not data: # หากอ่านข้อมูลไม่สำเร็จ
                break
            packet = GBNStruct(next_seq_num, next_seq_num, len(data), data)
            #สร้างอินสแตนซ์ GBNStruct ด้วยหมายเลขลำดับ next_seq_num หมายเลข acknowledgment next_seq_num 
            #ขนาดของข้อมูล payload len(data) และข้อมูล payload data
            packet.last_sent_time = time.time() #ตั้งค่าเวลาส่งล่าสุดของแพ็กเก็ตเป็นค่าปัจจุบัน
            packets.append(packet) #เพิ่มอินสแตนซ์ GBNStruct ลงในรายการ packets
            next_seq_num += 1 #เพิ่มหมายเลขลำดับถัดไป

    start_time = time.time() #เก็บเวลาเริ่มต้นของการถ่ายโอนไฟล์ในตัวแปร
    print(f"Total Packet: {len(packets)} \n") #พิมพ์จำนวนแพ็กเก็ตทั้งหมดในบรรทัดใหม่
    while base < len(packets): #ริ่มต้นการวนซ้ำหลัก โดยวนซ้ำจนกว่า base จะมีค่าเท่ากับจำนวนแพ็กเก็ตทั้งหมด
        for i in range(base, min(base + window_size, len(packets))):
            #วนซ้ำผ่านแพ็กเก็ตในหน้าต่างโดยเริ่มจาก base ไปยัง min(base + window_size, len(packets))
            if i == base or (i < next_seq_num and not packets[i].is_acknowledged):
                #ตรวจสอบว่าแพ็กเก็ต i เป็นแพ็กเก็ตแรกในหน้าต่างหรือเป็นแพ็กเก็ตที่ยังไม่ได้รับการ acknowledgment และหมายเลขลำดับน้อยกว่า next_seq_num
                if packets[i].is_acknowledged: #หากแพ็กเก็ต i ได้รับการ acknowledgment แล้ว ให้ส่งแพ็กเก็ตและอัปเดตเวลาส่งล่าสุด
                    client_socket.sendto(packets[i].serialize(), (server_ip, server_port))
                    packets[i].last_sent_time = time.time()

                elif time.time() - packets[i].last_sent_time >= timeout:
                    print(f"Sending packet {i}")
                    client_socket.sendto(packets[i].serialize(), (server_ip, server_port))
                    packets[i].last_sent_time = time.time()
                    #หากแพ็กเก็ต i ยังไม่ได้รับการ acknowledgment และเวลาผ่านไปนานกว่า timeout ให้พิมพ์ข้อความแจ้งการส่งแพ็กเก็ต i ส่งแพ็กเก็ตและอัปเดตเวลาส่งล่าสุด
                    if i == window_size + base - 1 or (len(packets) - base < window_size) and i == len(packets) - 1:
                        # ตรวจสอบว่าแพ็กเก็ต i เป็นแพ็กเก็ตสุดท้ายในหน้าต่างหรือเป็นแพ็กเก็ตสุดท้ายในรายการแพ็กเก็ตทั้งหมด
                        trys -= 1 #ลดจำนวนครั้งในการลองส่งลง 1
                        print(f"Trys more {trys} \n") #พิมพ์จำนวนครั้งในการลองส่งที่เหลือ
                    if trys == 0:
                        #หากจำนวนครั้งในการลองส่งเป็น 0 ให้พิมพ์ข้อความแจ้งว่าไม่มีการตอบสนองและหยุดการทำงานของโปรแกรม
                        print("No Response: Interrupted system call")
                        return

        try: #เริ่มต้นบล็อก try-except เพื่อจัดการข้อผิดพลาดเกี่ยวกับ timeout
            data, server_address = client_socket.recvfrom(1024) # รับข้อมูลและที่อยู่เซิร์ฟเวอร์จาก socket UDP โดยจำกัดขนาดข้อมูลไว้ที่ 1024 ไบต์
            ack_packet = GBNStruct.deserialize(data) #แปลงข้อมูลที่ได้รับเป็นอินสแตนซ์ GBNStruct
            if ack_packet is not None and hasattr(ack_packet, 'ack_no'):
                #ตรวจสอบว่า ack_packet ไม่ใช่ None และมี attribute ack_no ซึ่งหมายถึงหมายเลข acknowledgment
                ack_no = ack_packet.ack_no #ก็บหมายเลข acknowledgment จาก ack_packet
                if packets[ack_no].is_acknowledged is False: #ตรวจสอบว่าแพ็กเก็ต ack_no ยังไม่ได้รับการ acknowledgment
                    print(f"\nReceived ACK for packet {ack_no} {packets[ack_no].data}")
                    #พิมพ์ข้อความแจ้งการรับ acknowledgment สำหรับแพ็กเก็ต ack_no และข้อมูล payload ของแพ็กเก็ต
                    packets[ack_no].is_acknowledged = True
                    #ตั้งค่าสถานะ acknowledgment ของแพ็กเก็ต ack_no เป็น True
                    trys = 10 #รีเซ็ตจำนวนครั้งในการลองส่งเป็น 10
                    if ack_no == base: #ตรวจสอบว่า acknowledgment ที่ได้รับเป็นหมายเลขลำดับที่คาดไว้
                        while base < len(packets) and packets[base].is_acknowledged:
                            #เลื่อน base ไปข้างหน้าหากแพ็กเก็ตที่ base ได้รับการ acknowledgment แล้ว
                            base += 1 #ลื่อน base ไปข้างหน้าหนึ่งตำแหน่ง
        except socket.timeout: #จัดการข้อผิดพลาดเกี่ยวกับ timeout โดยข้ามไปโดยไม่ทำอะไร
            pass

    end_time = time.time() #เก็บเวลาสิ้นสุดของการถ่ายโอนไฟล์

    total_time = end_time - start_time #คำนวณเวลาถ่ายโอนไฟล์
    throughput = len(packets) / total_time #คำนวณ throughput เป็นจำนวนแพ็กเก็ตต่อวินาที
 
    print(f"\nFile transfer completed in {total_time:.2f} seconds.") #พิมพ์ข้อความแจ้งเวลาที่ใช้ในการถ่ายโอนไฟล์
    print(f"Throughput: {throughput:.2f} packets per second") #พิมพ์ข้อความแจ้ง throughput
    client_socket.close() #ปิด socket UDP

if True:
    main()
    #เรียกใช้ฟังก์ชัน main() เสมอเนื่องจากเงื่อนไข True เป็นจริงเสมอ
